Deepcode test project
PHP 5.6 + MySQL + Yii2 advanced template

Installation:

Open console, go to html folder - example cd /var/www/html

git init

git clone URL

composer install

open /var/www/html/deepcode/common/config/main-local.php and change database connection settings

php init

php yii migrate

Set virtual host for admin panel(if it is possible) to /var/www/html/deepcode/backend/web/index.php

Open it in browser, login to backend with login admin password 111111

Go to customers, add one or few customers.

Go to cards, add one or few cards, each card has one customer as owner.

Open console, cd /var/www/html/deepcode/ , then php yii help - you will see full list of console comands

php yii transaction-generator/add <card_id> <number transactions>
for example, php yii transaction-generator/add 5dd4ee73-9dec-4716-8d2e-835191b24bc7 10 - will add 10 transactions for card with id 5dd4ee73-9dec-4716-8d2e-835191b24bc7

Go to browser, transactions - you will see list of all added transactions, and you can search for them by month, card_id, type, exact amount
