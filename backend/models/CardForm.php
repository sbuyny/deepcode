<?php

namespace backend\models;

use Yii;
use common\models\Card;

/**
 * This is the model class for table "{{%card}}".
 *
 * @property int $id
 * @property string $card_id
 * @property string $type
 * @property int $number
 * @property string $name
 * @property int $expiration_month
 * @property int $expiration_year
 */
class CardForm extends Card
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_id', 'type', 'number', 'name', 'expiration_month', 'expiration_year', 'customer_id'], 'required'],
            [['expiration_month'], 'integer', 'min' => 1, 'max' => 12],
            [['expiration_year'], 'integer', 'min' => 1, 'max' => 30],
            [['card_id', 'type', 'name'], 'string', 'max' => 255],
            [['number'], 'integer'],
            [['number'], 'string', 'min' => 16, 'max' => 16],
            [['card_id'], 'unique'],
            [['customer_id'], 'integer'],
            [['number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card ID',
            'customer_id' => 'Customer',
            'type' => 'Card Type',
            'number' => 'Card Number',
            'name' => 'Name On Card',
            'expiration_month' => 'Expiration Month',
            'expiration_year' => 'Expiration Year',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
