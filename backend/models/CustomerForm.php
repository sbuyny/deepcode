<?php

namespace backend\models;

use Yii;
use common\models\Customer;

/**
 * This is the model class for table "{{%customer}}".
 *
 * @property int $id
 * @property string $first
 * @property string $last
 * @property string $middle
 * @property string $client_id
 * @property int $created_at
 * @property int $updated_at
 */
class CustomerForm extends Customer
{      
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first', 'last', 'client_id'], 'required'],
            [['first', 'last', 'middle', 'client_id'], 'string', 'max' => 255],
            [['client_id'], 'unique'],
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first' => 'First Name',
            'last' => 'Last Name',
            'middle' => 'Middle Name',
            'client_id' => 'Client ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}