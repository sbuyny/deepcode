<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $model common\models\Card */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'customer_id')->dropDownList(Customer::getCustomersList(), array('prompt' => 'Please, select')) ?>

    <?= $form->field($model, 'card_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'expiration_month')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'expiration_year')->textInput() ?>
        </div>
    </div>
    

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
