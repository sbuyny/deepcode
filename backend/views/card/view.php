<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $model common\models\Card */

$this->title = $model->card_id;
$this->params['breadcrumbs'][] = ['label' => 'Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'customer_id',
                'format'=>'raw',
                'value'  => call_user_func(function ($data) {
                $customer_id = $data->customer_id;
                    if(isset($customer_id)){
                        $customer = Customer::findOne($customer_id);
                        $username = $customer->first.' '.$customer->middle.' '.$customer->last;
                        return Html::a($username, "../customer/view?id=".$customer_id);
                    }
                }, $model),
            ],
            'id',
            'card_id',
            'type',
            'number',
            'name',
            'expiration_month',
            'expiration_year',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
