<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    
    <div  class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">Search</div>
        <div class="panel-body">
            <div class="input-group input-group-sm ">
                <div class="row">
                    <div class="col-xs-2">
                    <?= $form->field($model, 'month')
                             ->dropDownList(
                                     array_combine(range(1, 12), range(1, 12)), 
                                     array('prompt' => '')
                                     ) ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($model, 'card_id') ?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($model, 'amount') ?>
                    </div>
                    <div class="col-xs-2">
                        <?= $form->field($model, 'action_type')->dropDownList([
                            ''=>'',
                            'Debit' => 'Debit',
                            'Credit' => 'Credit',
                        ]) ?>
                    </div>
                    <div class="col-xs-2" style="margin-top:24px;">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
