<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Card;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'card_id',
                'format'=>'raw',
                'value'=>function ($data) {
                    return Html::a(Card::findOne($data->card_id)->card_id, "../card/view?id=".$data->card_id);
                },

            ],
            'time_stamp',
            'action_type',
            'amount',
        ],
    ]); ?>
</div>
