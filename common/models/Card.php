<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%card}}".
 *
 * @property int $id
 * @property string $card_id
 * @property string $type
 * @property int $number
 * @property string $name
 * @property int $expiration_month
 * @property int $expiration_year
 */
class Card extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%card}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card ID',
            'customer_id' => 'Customer',
            'type' => 'Card Type',
            'number' => 'Card Number',
            'name' => 'Name On Card',
            'expiration_month' => 'Expiration Month',
            'expiration_year' => 'Expiration Year',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    /**
     * Finds the Card model based on its card_id value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $card_id
     * @return Card model
     */
    public static function findByCardId($card_id)
    {
        return self::find()->where(['card_id' => $card_id])->one();
    }
}
