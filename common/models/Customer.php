<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%customer}}".
 *
 * @property int $id
 * @property string $first
 * @property string $last
 * @property string $middle
 * @property string $client_id
 * @property int $created_at
 * @property int $updated_at
 */
class Customer extends ActiveRecord
{   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
            ],
        ];
    }
    
    /**
     * Get customers list.
     *
     * @return array
     */
    public static function getCustomersList() {
        $customers = self::find()
                ->select(['id', 'first', 'middle', 'last'])
                ->orderBy('first', 'middle', 'last')
                ->all();

        return ArrayHelper::map($customers, 'id', 
                function($model, $defaultValue) {
                    return $model['first'].' '.$model['middle'].' '.$model['last'];
                });
    }
}
