<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property int $id
 * @property string $card_id
 * @property int $month
 * @property string $time_stamp
 * @property string $action_type
 * @property string $amount
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card ID',
            'month' => 'Month',
            'time_stamp' => 'Time Stamp',
            'action_type' => 'Action Type',
            'amount' => 'Amount',
        ];
    }
}
