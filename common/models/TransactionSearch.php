<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transaction;
use common\models\Card;
use yii\widgets\LinkPager;

/**
 * TransactionSearch represents the model behind the search form of `common\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month'], 'integer'],
            [['card_id', 'time_stamp', 'action_type'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 'pagination' => array('pageSize' => 10),
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'month' => $this->month,
            'amount' => $this->amount,
            'action_type' => $this->action_type,
        ]);
        
        if($this->card_id){
            $cardId = 'NULL';
            $card = Card::findByCardId($this->card_id);
            
            
            if($card){
                $cardId = $card->id; 
            }

            $query->andFilterWhere(['card_id' => $cardId]);
        }

        return $dataProvider;
    }
}
