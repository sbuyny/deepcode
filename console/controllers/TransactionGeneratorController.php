<?php

namespace console\controllers;

use Yii;
use console\models\TransactionGenerator;
use yii\console\Controller;
use yii\helpers\Console;
use common\models\Card;

/**
 * Transactions generation
 */
class TransactionGeneratorController extends Controller {

    /**
      * Required options - card id and number tramsactions to add (10, for example)
    */
    public function actionAdd($card_id,$count)
    {
        if (($card = Card::findByCardId($card_id)) === null) {
            echo $this->stdout("Can not find card by id.\n", Console::FG_RED, Console::BOLD);
            return false;
        }
        
        $added = 0;

        $transaction = new TransactionGenerator();
        
        for( $i=0; $i<$count; $i++ )
        {
          if ($transaction->AddTransaction($card->id)){
              $added++;
          }
        }
        
        if ($added > 0){
            echo $this->stdout($added . " transactions added to database.\n", Console::FG_GREEN, Console::BOLD);
            return true;
        } 
        
        echo $this->stdout("Can not add transactions.\n", Console::FG_RED, Console::BOLD);
    }

    /**
     * Optional - card id, else will remove all transactions
     */
    public function actionRemove($card_id = null) {
        
        if ($card_id && ($card = Card::findByCardId($card_id)) === null) {
            echo $this->stdout("Can not find card by id.\n", Console::FG_RED, Console::BOLD);
            return false;
        }
        
        $transaction = new TransactionGenerator();

        if ($card_id) {
            if ($transaction->RemoveTransactions(['card_id' => $card->id])) {
                echo $this->stdout("All transactions from card " . $card_id . " removed.\r\n", Console::FG_GREEN, Console::BOLD);
                return true;
            } 
            
            echo $this->stdout("Transactions from card " . $card_id . " not found.\r\n", Console::FG_RED, Console::BOLD);
            return false; 
        } 
        
        if ($transaction->RemoveTransactions()) {
            echo $this->stdout("All transactions removed.\n", Console::FG_GREEN, Console::BOLD);
            return true;
        } 
            
        echo $this->stdout("Transactions not found.\n", Console::FG_RED, Console::BOLD);
    }

}
