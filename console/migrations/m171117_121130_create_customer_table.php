<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m171117_121130_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'first' => $this->string()->notNull(),
            'last' => $this->string()->notNull(),
            'middle' => $this->string()->notNull(),
            'client_id' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('customer');
    }
}
