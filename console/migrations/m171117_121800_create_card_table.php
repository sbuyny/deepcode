<?php

use yii\db\Migration;

/**
 * Handles the creation of table `card`.
 */
class m171117_121800_create_card_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('card', [
            'id' => $this->primaryKey(),
            'card_id' => $this->string()->notNull()->unique(),
            'customer_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'number' => $this->bigInteger()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'expiration_month' => $this->integer(2)->notNull(),
            'expiration_year' => $this->integer(2)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        
        $this->createIndex(
            'idx-card-customer_id',
            'card',
            'customer_id'
        );
        
        $this->addForeignKey(
            'fk-card-customer_id',
            'card',
            'customer_id',
            'customer',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-card-customer_id',
            'card'
        );
        
        $this->dropIndex(
            'idx-card-customer_id',
            'card'
        );
        
        $this->dropTable('card');
    }
}
