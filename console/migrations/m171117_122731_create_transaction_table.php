<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 */
class m171117_122731_create_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'card_id' => $this->integer()->notNull(),
            'month' => $this->integer(2)->notNull(),
            'time_stamp' => $this->string()->notNull(),
            'action_type' => $this->string(6)->notNull(),
            'amount' => $this->decimal(19,2)->notNull(),
        ]);
        
        $this->addForeignKey(
            'fk-transaction-card_id',
            'transaction',
            'card_id',
            'card',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-transaction-card_id',
            'transaction'
        );
        
        $this->dropTable('transaction');
    }
}
