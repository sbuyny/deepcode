<?php

namespace console\models;

use Yii;
use common\models\Transaction;
use yii\base\Model;

class TransactionGenerator extends Transaction {


    public function AddTransaction($id)
    {
        $transaction = new Transaction();
        $transaction->card_id = $id;
        $random_time = mt_rand(time()-3600*24*30*3, time());
        $transaction->month = date('m', $random_time);
        $transaction->time_stamp = date('Y-m-dTm:h:s', $random_time);
        $action_types = ['Credit', 'Debit'];
        $transaction->action_type = $action_types[mt_rand(0, count($action_types) - 1)];
        $transaction->amount = mt_rand(1, 100000)/100;
        
        return $transaction->save() ? true : false;
    }
    
    public function RemoveTransactions($conditions = null)
    {
       return Transaction::deleteAll($conditions) ? true : false;
    }

}
